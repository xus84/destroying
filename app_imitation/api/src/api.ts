import express from 'express'
import cors from 'cors'
import {Request, Response} from 'express'
import {NoteService} from './services/notes'

const app = express()
app.use(express.json())
app.use(cors())

app.listen( 3000, ()=> {
    console.log('server started at 3000')
})
app.post('/saveNote', async (req: Request, res: Response) => {
    const note = req.body.note
    const title = req.body.title
    
     NoteService.save(note, title)
  
    
    res.status(200)
    res.send({})
})

app.post('/retrieveNote', async (req: Request, res: Response) => {
    const title = req.body.title
    
    const note = NoteService.retrieve(title)

    res.send(note)
})

declare global {
    namespace NodeJS {
        interface Global {
            __rootdir__ :  string ;
        }
    }
}
global.__rootdir__= __dirname  || process.cwd() 







